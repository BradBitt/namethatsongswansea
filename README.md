App Information
=====================
- App API = 26
- App Name = Name That Song Swansea
- Emulator Used = Pixel 3a
- Android Studio Version = 3.5.2
- Java Version = 1.8

View a demo of the mobile app at `\applicationDemo.mp4`.

Information for the user
=====================
Google Play Services on the App Store are required to play this App. This is because this App uses google maps
and the google maps API.

The App stores data within the phones internal storage. If this data becomes currupt the app will stop working.
Wiping the data on the mobile and running the app again will fix this issue.

Inside the `'/ProjectSource/'` folder includes a zip called 'AllSource.zip'.
This file contains the complete source of the android project.


Components used within this App
=====================
##### App Hotbar
The app hotbar is used for the game history activity. Tabs and fragments are used to seperate the two game types
into different lists.

##### Google maps
Google maps is used to display the missing lyrics that need to be found by the user. I have disabled the google maps UI controls
to make the UI cleaner and also to provent the user from zooming out of the map and dragging away from the playable zone.
The Lyrics are displayed on the map by using markers. The markers are displayed within a grid of co-ordinates and cannot appear outside
these boundaries. The users location and internet are required to use the google maps API and so the game cannot be played
without these permissions. The app requests the users location every second while a guessing game is running and the app is visible
on screen.

##### Material Design
Googles material design is used throughout the app. Buttons, floating buttons and dialog boxes are used.

##### Facebook API
The app also uses Facebook's API to allow the user to share either their total score, or the score for a specific game.
In order to do this I have registered the App as a Facebook developer project.

##### Recycler View
I have used 2 recycler views one for each tab fragment on the game history activity. These recycler views
are used to store and display lists of completed games of type current and type classic.

##### Internal storage
The app uses the mobile phones internal storage to save 3 different XML files to the device.

One XML contains the user information e.g. total score.
Another XML file is used to contain the progress of the previous game played.
The last XML file contains information for all the completed guessing games, so history of completed games is remembered.


Bonus Features I was not able to implement
=====================
I was unable to implement the bonus feature where if you `'long press'` on a marker, a menu will appear asking you if you wish to reset the location
of the pressed marker. After deeper investigation I discovered it was not possible to implement a callback for a `'long press'`
on a google map marker, only an on click. So I decided against implementing this bonus feature.


Classes
=====================
##### MasterActivity
This class is the super class for the other activities. It includes functions which are used by the other activities.

##### GameHistoryActivity
This class is an activity class and contains code to manage the game history activity.

##### GuessingGameActivity
This class is an activity used by the guessing game activity. This class contains all the functions for all the guessing game
buttons and is also used to invoke a GameSession object and a GameMap object.

##### HomeActivity
This class contains the code and functions for the home activity and its buttons.

##### GameContainerAdapter
This class contains the code for the Recycler View. It includes the functionality for the buttons within a game container,
which is an item within the recycler view list

##### TabsPagerAdapter
This class contains the code for each tab fragment within the game history activity. This allows the user to be able to switch
between fragments by clicking on the tab button.

##### ClassicFragment
This class contains the recycler view for the classic songs that have been completed already.

##### CurrentFragment
This class contains the recycler view for the currents songs that the user has completed.

##### MasterFragment
This class contains code that will be used by both the Classic fragment and the current fragment. It is the super class
to these two classes.

##### GameMap
A GameMap object contains code to initialise the google map and add the markers of lyrics to discover to the map.
An instance of this class is created for each guessing game that is played.

##### GameRunner
A GameRunner is an object class and just contains static functions. These functions are used to execute a guessing game.
It contains logic to execute a new game, a previous game or a replay of a game. It also contains functionality to ask the
user if they wish to play a new game or continue a game they have not yet completed.

##### GameSession
A GameSession class contains the logic for a guessing game. An instance of this is created for each guessing game.
The functionality this class has includes updating the UI, giving up the game and discovering a lyric.

##### Lyric
A lyric class is for each Lyric used within a guessing game. It includes useful information such as if a lyric has
been discovered or not, and also contains the co-ordinates object for the lyric too.

##### LyricContainer
A lyric container class contains some important information. It contains the lyric object, the Artist and the title for the song.
So with this object, all information about the song can be accessed.

##### LyricGame
This class contains information for a guessing game. It holds a LyricContainer object and also the score for a game.
It contains other functionality too, such as if a lyric has been discovered yet, or if you wish to reset a game or its score.

##### GameType
This is an enum class that contains the two different game types, classic and current. Its used with the LyricGame to distinguish
between the two different game types.

##### LyricLocationContainer
This object contains the co-ordinates for a lyric object. These co-ordinates will be used to display the lyric onto the map
if the lyric has not been discovered. The lyric co-ordinates are not saved inbetween sessions. These are created everytime the user
continues to play an existing game. The co-ordinates are randomly generated between two other static co-ordinates. The point of doing this
is so the random co-ordinates will always be somewhere around bay campus. 

##### PointValues
This class just contains static variables. These variables include the score values used to be awarded to the player for certain actions.
That means if you want to update the amount of points to receive for discovering a lyric, then you only need to update the value
within this class.

##### ArtistDialogClickListener
This class contains a listener for the artist dialogue box. So when a button in the dialogue box is clicked this class is called.
It contains the functionality for checking to see if the users guess of an artist name was correct or not.

##### GameDialogNewGameClickListener
This class contains the functionality for the click listener of a dialogue box for playing a new game.
This is only used if the user has a previous game saved and is trying to play a new game.

##### GameDialogWithGameClickListener
This class contains the functionality for the click listener of a dialogue box for replaying a game.
This is only used if the user has a previous game saved and is trying to replay a game.

##### GiveUpDialogClickListener
This class is called when the user clicks the give up button within the guessing game activity. This is used for
the button click listener within the give up dialogue window when the user chooses to give up on a guessing game.

##### GuessDialogClickListener
This class is the click listener for when the user has clicked the guess button and has entered in a song title.
It contains the functionality to check to see if the title entered matches the title of the song.

##### GameManager
This is a static object that contains a list of all the lyric games. It contains useful functions, such as 
getting all uncompleted songs or all completed songs.

##### PreviousGameManager
This static class contains the current previous game loaded. This is so its easy to access the previous game played.

##### ResourceManager
This is a static class that is used when the app is started. Its used to execute the loading or saving of all the XML files
on the app.

##### UserInfoManager
This is a static class that is used to contain the information about the user, such as the users total score.

##### FileLoader
This is a static class that contains code to load and read the the songs that are saved in the assets folder of the app.
It doesn't just read the data but uses the lyric factory to create all the LyricGame objects.

##### LyricFactory
This static class is used to create a LyricGame object. It gathers the artist name and song title from the text file and
also randomly selects a snippet of lyrics to use from the song text file.

##### Permissions
This static class contains functionality to ask the user for permissions and to check the permissions exist.
This class exists to prevent duplication of code.

##### XMLFileReader
This static class contains all the code for reading in the different XML files.

##### XMLFileWriter
This static class contains all the code for writing the objects from the app to the XML files on the device's internal storage.

##### XMLValues
This static class contains all the static variables of tag names and values names to use when writing and reading XML files.
If I wanted to update any tag names, then I only have to update this class.
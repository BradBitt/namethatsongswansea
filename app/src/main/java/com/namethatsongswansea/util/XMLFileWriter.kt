package com.namethatsongswansea.util

import com.namethatsongswansea.game.lyric.LyricGame
import com.namethatsongswansea.resources.GameManager
import com.namethatsongswansea.resources.UserInfoManager
import org.w3c.dom.Document
import org.w3c.dom.Element
import java.io.File
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

object XMLFileWriter {

    /**
     * Writes the user info from the user info manager
     * to an xml file on the users mobile device.
     */
    fun writeUserInfo(file: File) {

        // Creates the XML document
        val doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument()
        // Create the root tag
        val userInfoElement: Element = doc.createElement(XMLValues.userInfoTag)
        doc.appendChild(userInfoElement)

        // Create an element, applies some text to it
        // Then assigns it as a child to the parent tag.
        val scoreElem: Element = doc.createElement(XMLValues.totalScore)
        scoreElem.appendChild(doc.createTextNode(UserInfoManager.getScore().toString()))
        userInfoElement.appendChild(scoreElem)

        // Writes the xml to the output stream
        writeToOutputStream(doc, file)
    }

    /**
     * Writes the game history from the game history manager
     * to an xml file on the users mobile device.
     */
    fun writeGameHistory(file: File) {

        // Creates the XML document
        val doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument()
        // Create the root tag
        val rootGameElement: Element = doc.createElement(XMLValues.rootGameTag)
        doc.appendChild(rootGameElement)

        // Gets all the completed games
        val completedLyricGames = GameManager.getCompletedGames()
        // Loops through all the completed games and writes them
        for (completedGame in completedLyricGames) {

            // Creates the game element
            // Then appends it to the root.
            val gameElement: Element = doc.createElement(XMLValues.gameTag)
            rootGameElement.appendChild(gameElement)

            // Adds all the children nodes
            addGameHistoryChildElements(gameElement, doc, completedGame)
        }

        // Writes the xml to the output stream
        writeToOutputStream(doc, file)
    }

    /**
     * Saves the previous game as an xml file.
     */
    fun writePreviousGame(file: File, prevGame: LyricGame) {

        // Creates the XML document
        val doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument()
        // Create the root tag
        val previousGameElement: Element = doc.createElement(XMLValues.previousGameTag)
        doc.appendChild(previousGameElement)

        // Adds all the child nodes.
        addGameHistoryChildElements(previousGameElement, doc, prevGame)

        // This adds elements which are specific just for previous games.
        addPreviousGameSpecificElements(previousGameElement, doc, prevGame)

        // Writes the xml to the output stream
        writeToOutputStream(doc, file)
    }

    /**
     * Makes the XML prettier and then transforms it to the
     * Output stream.
     */
    private fun writeToOutputStream(doc: Document, file: File) {

        // Create the xml file
        // Transform the DOM Object to an XML File
        val transformerFactory = TransformerFactory.newInstance()
        val transformer = transformerFactory.newTransformer()
        val domSource = DOMSource(doc)
        transformer.transform(domSource, StreamResult(file))
    }

    /**
     * This function adds all the child elements
     * to the correct game element
     */
    private fun addGameHistoryChildElements(
        gameElement: Element,
        doc: Document,
        gameLyric: LyricGame
    ) {
        // Artist
        val artistElem: Element = doc.createElement(XMLValues.artist)
        artistElem.appendChild(doc.createTextNode(gameLyric.lyricContainer.artist))
        gameElement.appendChild(artistElem)

        // Score
        val scoreElem: Element = doc.createElement(XMLValues.score)
        scoreElem.appendChild(doc.createTextNode(gameLyric.getScore().toString()))
        gameElement.appendChild(scoreElem)

        // Song Title
        val songTitleElem: Element = doc.createElement(XMLValues.songTitle)
        songTitleElem.appendChild(doc.createTextNode(gameLyric.lyricContainer.title))
        gameElement.appendChild(songTitleElem)

        // Lyrics
        val lyricElem: Element = doc.createElement(XMLValues.lyrics)
        lyricElem.appendChild(doc.createTextNode(gameLyric.lyricContainer.lyricSnippet))
        gameElement.appendChild(lyricElem)

        // Game Type
        val gameTypeElem: Element = doc.createElement(XMLValues.gameType)
        gameTypeElem.appendChild(doc.createTextNode(gameLyric.gameType.toString()))
        gameElement.appendChild(gameTypeElem)

        // Game Completed
        val gameCompletedElem: Element = doc.createElement(XMLValues.gameCompleted)
        gameCompletedElem.appendChild(doc.createTextNode(gameLyric.isCompleted.toString()))
        gameElement.appendChild(gameCompletedElem)

        // Game Artist Completed
        val gameArtistCompletedElem: Element = doc.createElement(XMLValues.gameArtistCompleted)
        gameArtistCompletedElem.appendChild(doc.createTextNode(gameLyric.isArtistCorrect.toString()))
        gameElement.appendChild(gameArtistCompletedElem)
    }

    /**
     * This function adds additional xml elements. These elements are specific
     * for a previous game.
     */
    private fun addPreviousGameSpecificElements(
        gameElement: Element,
        doc: Document,
        gameLyric: LyricGame
    ) {

        // Weather a lyricContainer has already been discovered.
        val discoveredAtLeastOneElem: Element = doc.createElement(XMLValues.discoveredAtLeastOne)
        discoveredAtLeastOneElem.appendChild(doc.createTextNode(gameLyric.hasFirstLyricBeenDiscovered.toString()))
        gameElement.appendChild(discoveredAtLeastOneElem)

        // Adds an element for each lyric
        for (lyric in gameLyric.lyricContainer.lyrics) {
            val lyricBoxElem: Element = doc.createElement(XMLValues.lyricTag)

            // Adds the Id
            val idElem = doc.createElement(XMLValues.lyricId)
            idElem.appendChild(doc.createTextNode(lyric.id.toString()))
            lyricBoxElem.appendChild(idElem)

            // Adds the discovered tag
            val discoveredElem = doc.createElement(XMLValues.lyricDiscovered)
            discoveredElem.appendChild(doc.createTextNode(lyric.discovered.toString()))
            lyricBoxElem.appendChild(discoveredElem)

            gameElement.appendChild(lyricBoxElem)
        }
    }
}
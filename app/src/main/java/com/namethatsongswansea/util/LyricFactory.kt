package com.namethatsongswansea.util

import com.namethatsongswansea.game.lyric.LyricContainer
import java.io.BufferedReader
import java.io.InputStream

object LyricFactory {

    /**
     * Takes in an input stream and the file name.
     * It then uses these two parameters to create and return a lyricContainer object.
     */
    fun createLyric(inputStream: InputStream, file: String): LyricContainer {

        // Uses the file name to get the artist
        // I use the regex to return the artist and song name in an array.
        // Due to the format of the filename, I know that the artist name will
        // always be index 0 and the song title will be index 1. "[a-z_]*"
        val regexList = file.split(Regex("\\(|\\)|.txt"))

        val artistName = regexList[0].replace("_", " ")
        val songTitle = regexList[1].replace("_", " ")
        val lyricSnippet = getRandomLyricSnippet(inputStream)

        // Creates and returns the lyricContainer.
        return LyricContainer(
            songTitle,
            artistName,
            lyricSnippet
        )
    }

    /**
     * This function takes in an input stream and then
     * returns a random sentence from the input stream.
     */
    private fun getRandomLyricSnippet(inputStream: InputStream): String {

        val allLines = arrayListOf<String>()
        val reader = BufferedReader(inputStream.reader())

        // This is equivalent to try-finally
        // Reads in every line.
        reader.use {

            reader.readText().split("\n").forEach {
                allLines.add(it)
            }
        }

        // Shuffles the array and then returns the first acceptable
        // line of lyrics.
        allLines.shuffle()
        for (lyricLine in allLines) {
            if (isLyricLineValid(lyricLine)) {
                return lyricLine
            }
        }

        // If no valid lyrics can be found then throw an exception.
        throw Exception("No valid lyricContainer snippet found!")
    }

    /**
     * This function takes in a string of lyrics.
     * It then assess whether this string is acceptable to
     * be used within the guessing game. If it is then it
     * returns true.
     */
    private fun isLyricLineValid(lyricLine: String): Boolean {

        var isValid = true

        // This checks to see if the strings length is not between
        // 20 and 30 characters long. If its not then set the boolean to false.
        // This is so we don't have a too long sentence within the guessing game.
        if (lyricLine.length !in 15..35) {
            isValid = false
        }

        // Checks that the sentence contains at least 3 words
        // If it doesn't contain at least 3 words then set the boolean to false.
        if (lyricLine.split(" ").size < 3) {
            isValid = false
        }

        return isValid
    }
}
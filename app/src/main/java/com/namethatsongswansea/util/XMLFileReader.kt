package com.namethatsongswansea.util

import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.namethatsongswansea.R
import com.namethatsongswansea.game.GameType
import com.namethatsongswansea.game.lyric.LyricContainer
import com.namethatsongswansea.game.lyric.LyricGame
import com.namethatsongswansea.resources.GameManager
import com.namethatsongswansea.resources.PreviousGameManager
import com.namethatsongswansea.resources.UserInfoManager
import org.w3c.dom.Document
import java.io.File
import java.util.*
import javax.xml.parsers.DocumentBuilderFactory

object XMLFileReader {

    /**
     * Reads in values found within the user info xml file
     * and then loads them into the user info manager.
     */
    fun setUserInfo(file: File, activity: AppCompatActivity) {

        // Checks to see if the file given is not valid.
        if (!isFileValid(file, "User Info", activity)) {
            return
        }

        val doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file)
        val userInfoNode = doc.getElementsByTagName(XMLValues.userInfoTag).item(0)

        // Loops through all the child elements
        for (j in 0 until userInfoNode.childNodes.length) {

            val childNode = userInfoNode.childNodes.item(j)

            when (childNode.nodeName) {
                XMLValues.totalScore -> UserInfoManager.loadScore((childNode.textContent).toInt())
            }
        }
    }

    /**
     * Reads in values found within the game history xml file
     * and then loads them into the game history manager.
     */
    fun setGameHistoryInfo(file: File, activity: AppCompatActivity) {

        // Checks to see if the file given is not valid.
        if (!isFileValid(file, "Game History", activity)) {
            return
        }

        val doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file)
        val gameNodes = doc.getElementsByTagName(XMLValues.gameTag)

        for (i in 0 until gameNodes.length) {

            val gameNode = gameNodes.item(i)

            // The variables that need to be assigned to.
            lateinit var artistName: String
            var gameScore = 0
            lateinit var gameSongTitle: String
            lateinit var gameLyrics: String
            lateinit var gameType: String
            var gameComplete = false
            var gameArtistComplete = false

            // Loops through all the child elements
            for (j in 0 until gameNode.childNodes.length) {

                val childNode = gameNode.childNodes.item(j)

                when (childNode.nodeName) {
                    XMLValues.artist -> artistName = childNode.textContent
                    XMLValues.score -> gameScore = childNode.textContent.toInt()
                    XMLValues.songTitle -> gameSongTitle = childNode.textContent
                    XMLValues.lyrics -> gameLyrics = childNode.textContent
                    XMLValues.gameType -> gameType = childNode.textContent
                    XMLValues.gameCompleted -> gameComplete = childNode.textContent.toBoolean()
                    XMLValues.gameArtistCompleted -> gameArtistComplete =
                        childNode.textContent.toBoolean()
                }
            }

            // Creates the lyricContainer object and
            // creates a LyricContainer Game object.
            val lyric = LyricContainer(
                gameSongTitle,
                artistName,
                gameLyrics
            )
            GameManager.addCompletedGame(
                LyricGame(
                    gameScore,
                    lyric,
                    GameType.valueOf(gameType),
                    gameComplete,
                    gameArtistComplete
                )
            )
        }
    }

    /**
     * Loads the previous game information from an XML file
     * then stores it in the previous game manager.
     */
    fun setPreviousGame(file: File, activity: AppCompatActivity) {

        // Checks to see if the file given is not valid.
        if (!isFileValid(file, "Previous Game", activity)) {
            return
        }

        val doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file)
        val previousGameNode = doc.getElementsByTagName(XMLValues.previousGameTag).item(0)

        // The variables that need to be assigned to.
        lateinit var artistName: String
        var gameScore = 0
        lateinit var gameSongTitle: String
        lateinit var gameLyrics: String
        lateinit var gameType: String
        var gameComplete = false
        var gameArtistComplete = false
        var hasFirstLyricBeenDiscovered = false

        // Loops through all the child elements
        for (j in 0 until previousGameNode.childNodes.length) {

            val childNode = previousGameNode.childNodes.item(j)

            when (childNode.nodeName) {
                XMLValues.artist -> artistName = childNode.textContent
                XMLValues.score -> gameScore = childNode.textContent.toInt()
                XMLValues.songTitle -> gameSongTitle = childNode.textContent
                XMLValues.lyrics -> gameLyrics = childNode.textContent
                XMLValues.gameType -> gameType = childNode.textContent
                XMLValues.gameCompleted -> gameComplete = childNode.textContent.toBoolean()
                XMLValues.gameArtistCompleted -> gameArtistComplete =
                    childNode.textContent.toBoolean()
                XMLValues.discoveredAtLeastOne -> hasFirstLyricBeenDiscovered =
                    childNode.textContent.toBoolean()
            }
        }

        // Creates the lyricContainer.
        val lyric =
            LyricContainer(gameSongTitle, artistName, gameLyrics)

        // Loads all the lyricContainer Boxes and applies weather or not the lyricContainer has been discovered.
        addDiscoveredLyrics(doc, lyric)

        // Makes the LyricContainer game.
        val lyricGame = LyricGame(
            gameScore,
            lyric,
            GameType.valueOf(gameType),
            gameComplete,
            gameArtistComplete
        )

        // Adds additional values.
        lyricGame.hasFirstLyricBeenDiscovered = hasFirstLyricBeenDiscovered

        // Stores it as the previous game manager object.
        PreviousGameManager.setRecentGame(lyricGame)
    }

    /**
     * This function takes in the lyric container and reads and updates
     * the lyrics that have been discovered.
     */
    private fun addDiscoveredLyrics(doc: Document, lyricContainer: LyricContainer) {

        // Finds all lyric tags
        val allLyrics = doc.getElementsByTagName(XMLValues.lyricTag)

        // Loops through all the lyric tags
        for (i in 0 until allLyrics.length) {

            // Gets a lyric tag from the list
            val lyricElem = allLyrics.item(i)

            // The values needed to find the update the lyricContainer box.
            var lyricId = 0
            var lyricDiscovered = false

            // Loops through all the child elements
            for (j in 0 until lyricElem.childNodes.length) {

                val childNode = lyricElem.childNodes.item(j)

                when (childNode.nodeName) {
                    XMLValues.lyricId -> lyricId = childNode.textContent.toInt()
                    XMLValues.lyricDiscovered -> lyricDiscovered =
                        childNode.textContent.toBoolean()
                }
            }

            // Finds the lyric within the lyricContainer and updates its properties.
            for (lyric in lyricContainer.lyrics) {

                // If the id's match then update the lyricContainer.
                if (lyric.id == lyricId) {
                    lyric.discovered = lyricDiscovered
                }
            }
        }
    }

    /**
     * Checks to see if the file given is valid.
     */
    private fun isFileValid(file: File, info:String, activity: AppCompatActivity): Boolean {

        // Checks if the file is empty or not.
        var content = ""
        val scanner = Scanner(file)
        while (scanner.hasNextLine()) {
            content += scanner.nextLine()
        }
        if (content.isEmpty()) {

            // Displays a warning message that data was lost.
            MaterialAlertDialogBuilder(activity)
                .setTitle(R.string.loading_data_error_title)
                .setMessage(info + activity.resources.getString(R.string.loading_data_error_message_end))
                .show()
            return false
        }

        return true
    }
}
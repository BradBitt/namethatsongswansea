package com.namethatsongswansea.util

object XMLValues {

    // XML values used for user info
    const val userInfoTag: String = "userinfo"
    const val totalScore: String = "totalscore"

    // XML values used for game history and
    // XML values for previous game
    const val rootGameTag: String = "games"
    const val gameTag: String = "game"
    const val artist: String = "artist"
    const val score: String = "score"
    const val songTitle: String = "songTitle"
    const val lyrics: String = "lyrics"
    const val gameType: String = "gameType"
    const val gameCompleted: String = "gameCompleted"
    const val gameArtistCompleted: String = "gameArtistCompleted"

    // Previous game specific tags
    const val previousGameTag: String = "previousGame"
    const val lyricTag: String = "lyric"
    const val lyricId: String = "lyricId"
    const val lyricDiscovered: String = "lyricDiscovered"
    const val discoveredAtLeastOne: String = "discoveredAtLeastOne"
}
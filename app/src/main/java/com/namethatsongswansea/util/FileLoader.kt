package com.namethatsongswansea.util

import com.namethatsongswansea.game.lyric.LyricContainer
import com.namethatsongswansea.resources.ResourceManager
import java.io.InputStream

object FileLoader {

    /**
     * Loads all the songs into lyricContainer objects by searching the directory
     * provided.
     */
    fun loadSongs(directory: Array<String>?, parentDir: String?): Array<LyricContainer> {

        // Checks the directory is not null
        if (directory == null) {
            return arrayOf<LyricContainer>()
        }

        var allLyrics = arrayListOf<LyricContainer>()

        // Loops through all the files within the directory
        for (file in directory) {
            // Loads files
            var inputStream = loadFile(parentDir, file)

            // Creates a LyricContainer
            val lyric = LyricFactory.createLyric(inputStream, file)

            // Adds lyricContainer to Array
            allLyrics.add(lyric)
        }

        return allLyrics.toTypedArray()
    }

    /**
     * Loads the file provided into an input stream.
     */
    private fun loadFile(parentDir: String?, fileName: String): InputStream {
        if (parentDir == null) {
            return ResourceManager.allAssets.open(fileName)
        }
        return ResourceManager.allAssets.open("$parentDir/$fileName")
    }
}
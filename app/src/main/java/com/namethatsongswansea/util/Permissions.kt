package com.namethatsongswansea.util

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.namethatsongswansea.game.GameMap

object Permissions {

    /**
     * This function asks the user for the required permissions.
     */
    fun ask(activity: Activity) {

        // Checks if we have permissions already.
        if (!check(activity)) {

            // Request them
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                GameMap.LOCATION_PERMISSION_REQUEST_CODE
            )
        }
    }

    /**
     * This function checks to see if the required permissions have been given before
     * starting a game.
     */
    fun check(activity: Activity): Boolean {

        // if true then permission has not been given.
        if (ContextCompat.checkSelfPermission(
                activity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            return false
        }

        // Permissions have been given.
        return true
    }
}
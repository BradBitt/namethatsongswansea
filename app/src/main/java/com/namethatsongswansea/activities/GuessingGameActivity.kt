package com.namethatsongswansea.activities

import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.namethatsongswansea.game.GameMap
import com.namethatsongswansea.game.GameSession
import com.namethatsongswansea.game.lyric.Lyric
import com.namethatsongswansea.game.PointValues
import com.namethatsongswansea.game.listener.ArtistDialogClickListener
import com.namethatsongswansea.game.listener.GiveUpDialogClickListener
import com.namethatsongswansea.game.listener.GuessDialogClickListener
import com.namethatsongswansea.resources.GameManager
import com.namethatsongswansea.resources.PreviousGameManager
import com.namethatsongswansea.resources.UserInfoManager

class GuessingGameActivity : AppCompatActivity(), OnMapReadyCallback {

    // The game session that belongs to this activity.
    lateinit var session: GameSession
    lateinit var gameMap: GameMap

    /**
     * This function is called when the activity is created.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.namethatsongswansea.R.layout.guessing_game_activity)

        // Checks the data that was passed over.
        val intent = intent
        val lyricGameTitle = intent.getStringExtra(GameManager.LYRIC_GAME_TITLE_KEY)

        // Checks to see if the game passed is a previous game,
        // if it is then load the previous game instead.
        val game = GameManager.findGameByTitle(lyricGameTitle!!)

        // Resets the games values if its been completed already.
        // This case only occurs when the user wishes to replay a completed game.
        if (game!!.isCompleted) {
            game.reset()
        }

        // Makes the game session
        session = GameSession(this, game)

        // Creates a GameMap object.
        // This object holds the map and its settings used for this game
        val mapFragment =
            supportFragmentManager.findFragmentById(com.namethatsongswansea.R.id.map_view) as SupportMapFragment
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mapFragment.getMapAsync(this)
        gameMap = GameMap(fusedLocationClient, this)
    }

    /**
     * Allocates the map variable.
     * Sets controls for the map.
     * The sets up the map ready to be used by the user.
     */
    override fun onMapReady(googleMap: GoogleMap) {

        // Initializes the map.
        gameMap.initializeMap(googleMap)

        // Adds the missing lyrics.
        gameMap.addMissingLyrics(session)
    }

    /**
     * Overrides finish to include saving the previous game.
     */
    override fun finish() {

        // Sets the previous game
        if (session.lyricGame != null) {
            PreviousGameManager.setRecentGame(session.lyricGame!!)
        }

        // Stops requesting map updates
        gameMap.stopAndDestroyMap()

        super.finish()
    }

    /**
     * This function is called when the user clicks the artist button.
     * It allows the user to make a guess of the artist name.
     */
    @Suppress("UNUSED_PARAMETER")
    fun artistButtonClicked(view: View) {

        // If the artist is already complete then update UI and quit function.
        if (session.lyricGame!!.isArtistCorrect) {
            session.updateGameUI()
            return
        }

        // Creates the input used to collect the artist name
        val artistInput = EditText(this)
        artistInput.inputType = InputType.TYPE_CLASS_TEXT

        // Opens the dialog to to enter in the artist name.
        MaterialAlertDialogBuilder(this)
            .setTitle(getString(com.namethatsongswansea.R.string.name_artist_dialog_title))
            .setPositiveButton(
                getString(com.namethatsongswansea.R.string.name_artist_dialog_positive_button),
                ArtistDialogClickListener(this, artistInput, session.lyricGame!!)
            )
            .setNegativeButton(
                getString(com.namethatsongswansea.R.string.name_artist_dialog_negative_button),
                null
            )
            .setView(artistInput)
            .show()
    }

    /**
     * This function is called when the user clicks the help button.
     * This function reveals a missing word for the user.
     */
    @Suppress("UNUSED_PARAMETER")
    fun helpButtonClicked(view: View) {

        // If all the lyrics have been found then update UI and quit function.
        if (session.lyricGame!!.areAllLyricsFound()) {
            session.updateGameUI()
            return
        }

        // Creates a list of lyrics and shuffles them to make it random.
        val lyrics = arrayListOf<Lyric>()
        for (lyric in session.lyricGame!!.lyricContainer.lyrics) {
            lyrics.add(lyric)
        }
        lyrics.shuffle()

        // Loops through the array of shuffled lyrics
        for (lyric in lyrics) {

            // finds the first undiscovered lyric and then reveals it and updates the UI.
            if (!lyric.discovered) {
                lyric.discovered = true
                gameMap.removeDiscoveredLyric(lyric)
                break
            }
        }

        // Updates the points and UI
        session.lyricGame!!.hasFirstLyricBeenDiscovered = true
        session.lyricGame!!.updateScore(PointValues.ASKED_FOR_HELP)
        session.updateGameUI()
    }

    /**
     * This function is called when the user clicks the give up button.
     * This function quits the game and doesn't save it as the user
     * has given up.
     */
    @Suppress("UNUSED_PARAMETER")
    fun giveUpButtonClicked(view: View) {

        MaterialAlertDialogBuilder(this)
            .setTitle(this.getString(com.namethatsongswansea.R.string.give_up_dialog_title))
            .setPositiveButton(
                com.namethatsongswansea.R.string.give_up_dialog_give_up,
                GiveUpDialogClickListener(this, session.lyricGame!!)
            )
            .setNegativeButton(com.namethatsongswansea.R.string.give_up_dialog_dont_give_up, null)
            .show()
    }

    /**
     * This function is called when the user clicks the
     * exit button.
     */
    @Suppress("UNUSED_PARAMETER")
    fun exitButtonClicked(view: View) {
        finish()
    }

    /**
     * This function is called when the user clicks the guess button.
     * This allows the user to make a guess on the lyricContainer title.
     */
    @Suppress("UNUSED_PARAMETER")
    fun guessButtonClicked(view: View) {

        // Creates the input used to collect the lyricContainer title
        val titleInput = EditText(this)
        titleInput.inputType = InputType.TYPE_CLASS_TEXT

        // Opens the dialog to to enter in the artist name.
        MaterialAlertDialogBuilder(this)
            .setTitle(getString(com.namethatsongswansea.R.string.guess_button_dialog_title))
            .setPositiveButton(
                getString(com.namethatsongswansea.R.string.guess_button_dialog_positive_button),
                GuessDialogClickListener(this, titleInput, session.lyricGame!!)
            )
            .setNegativeButton(
                getString(com.namethatsongswansea.R.string.guess_button_dialog_negative_button),
                null
            )
            .setView(titleInput)
            .show()
    }


    /**
     * This function is called when the user has clicked the complete button.
     * This completes the game, adds the score to the total and quits the game.
     */
    @Suppress("UNUSED_PARAMETER")
    fun completeButtonClicked(view: View) {

        // Adds the game score to the total score.
        UserInfoManager.appendScore(session.lyricGame!!.getScore())

        // Move game over to gameHistory manager.
        GameManager.addCompletedGame(session.lyricGame!!)

        // Sets the game to null so the game can be quit without being saved.
        session.lyricGame = null

        // removes the game and quits
        session.removeGame()
    }
}
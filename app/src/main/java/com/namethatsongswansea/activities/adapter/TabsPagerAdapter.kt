package com.namethatsongswansea.activities.adapter

import android.app.Activity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.namethatsongswansea.activities.fragment.ClassicFragment
import com.namethatsongswansea.activities.fragment.CurrentFragment

class TabsPagerAdapter(
    fm: FragmentManager,
    private val tabTitles: Array<String>,
    private val activity: Activity
) : FragmentStatePagerAdapter(fm) {

    /**
     * Supplies instances of 'ScreenSlidePageFragment' as new pages
     */
    override fun getItem(index: Int): Fragment {
        when (index) {
            0 -> return CurrentFragment(activity)
            1 -> return ClassicFragment(activity)
        }
        return CurrentFragment(activity)
    }

    /**
     * Returns the amount of pages the adapter will create
     */
    override fun getCount(): Int {
        return tabTitles.size
    }

    /**
     * return the titles that appear on tabs
     */
    override fun getPageTitle(position: Int): CharSequence? {
        return tabTitles[position]
    }
}
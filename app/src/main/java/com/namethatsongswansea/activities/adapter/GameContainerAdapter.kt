package com.namethatsongswansea.activities.adapter

import android.app.Activity
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import com.google.android.material.snackbar.Snackbar
import com.namethatsongswansea.R
import com.namethatsongswansea.game.GameRunner
import com.namethatsongswansea.game.lyric.LyricGame
import com.namethatsongswansea.resources.UserInfoManager

class GameContainerAdapter(
    private val songViewArrayList: MutableList<LyricGame>,
    private val activity: Activity
) :
    RecyclerView.Adapter<GameContainerAdapter.ViewHolder>() {

    /**
     * A class that holds the information from the recycler view xml template.
     */
    class ViewHolder(layout: View) : RecyclerView.ViewHolder(layout) {

        val titleLabel = layout.findViewById<View>(R.id.title_label) as TextView
        val artistLabel = layout.findViewById<View>(R.id.artist_label) as TextView
        val scoreLabel = layout.findViewById<View>(R.id.score_label) as TextView
        val shareButton = layout.findViewById<View>(R.id.share_score_button) as Button
        val replayButton = layout.findViewById<View>(R.id.replay_button) as Button
    }

    /**
     * Reads in the Game history container which is found in a separate xml file.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        // Creates the view holder
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.game_history_game_container, parent, false)
        return ViewHolder(v)
    }

    /**
     * Sets the values for the UI from the current Game based on the position parameter.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val lyricGame: LyricGame = songViewArrayList[position]

        // Sets the UI elements with the values from the object.
        holder.titleLabel.text = lyricGame.lyricContainer.title
        holder.artistLabel.text = lyricGame.lyricContainer.artist
        holder.scoreLabel.text = lyricGame.getScore().toString()

        // Creates the onClick function for the share button
        holder.shareButton.setOnClickListener {
            shareGameScore(holder)
        }

        // Creates the onClick function for the replay button
        holder.replayButton.setOnClickListener {
            GameRunner.startGameWithGame(holder.itemView.context, lyricGame)
        }
    }

    /**
     * This function returns the number of elements to produce.
     */
    override fun getItemCount(): Int {
        return songViewArrayList.size
    }

    /**
     * This function is called to share the game score
     */
    private fun shareGameScore(holder: ViewHolder) {
        val shareDialog = ShareDialog(activity)

        val artistLabel = activity.resources.getString(R.string.recycler_view_song_title)
        val titleLabel = activity.resources.getString(R.string.recycler_view_song_artist)

        // Builds the string
        val stringBuilder = StringBuilder()
        stringBuilder.append(holder.itemView.context.getString(R.string.share_game_score_message))
        stringBuilder.append(" ")
        stringBuilder.append(UserInfoManager.getScore().toString())
        stringBuilder.append("\n")
        stringBuilder.append(artistLabel)
        stringBuilder.append(" ")
        stringBuilder.append(holder.titleLabel.text.toString())
        stringBuilder.append("\n")
        stringBuilder.append(titleLabel)
        stringBuilder.append(" ")
        stringBuilder.append(holder.artistLabel.text.toString())

        // Creates the share object and opens it.
        val linkContent = ShareLinkContent.Builder()
            .setQuote(stringBuilder.toString())
            .setContentUrl(Uri.parse(holder.itemView.context.getString(R.string.bitbucket_link)))
            .build()
        try {
            shareDialog.show(linkContent)
        } catch (e: Exception) {
            Snackbar.make(
                holder.itemView,
                holder.itemView.context.getString(R.string.facebook_share_fail),
                Snackbar.LENGTH_LONG
            ).show()
        }
    }
}
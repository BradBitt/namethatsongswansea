package com.namethatsongswansea.activities.fragment

import android.app.Activity
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.namethatsongswansea.activities.adapter.GameContainerAdapter
import com.namethatsongswansea.game.GameType
import com.namethatsongswansea.game.lyric.LyricGame
import com.namethatsongswansea.resources.GameManager

object MasterFragment {

    /**
     * Sets up the recycler view for the game history activity.
     */
    fun setupRecyclerView(
        gameType: GameType,
        recyclerView: RecyclerView,
        view: View,
        activity: Activity
    ) {

        val filteredGames = getGameTypeListCompletedGames(gameType)

        // Sets the layouts and adapters
        recyclerView.layoutManager = LinearLayoutManager(view.context)
        recyclerView.adapter = GameContainerAdapter(filteredGames, activity)
    }

    /**
     * Returns a list of games with the game type 'current'.
     */
    private fun getGameTypeListCompletedGames(gameType: GameType): ArrayList<LyricGame> {

        val gameTypeList = arrayListOf<LyricGame>()
        GameManager.getCompletedGames().forEach {
            if (it.gameType == gameType) {
                gameTypeList.add(it)
            }
        }
        return gameTypeList
    }
}
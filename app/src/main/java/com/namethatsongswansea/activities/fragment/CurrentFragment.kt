package com.namethatsongswansea.activities.fragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.namethatsongswansea.R
import com.namethatsongswansea.activities.fragment.MasterFragment.setupRecyclerView
import com.namethatsongswansea.game.GameType

class CurrentFragment(private val activity: Activity) : Fragment() {

    /**
     * Returns the Recycler View from the separate xml file.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return inflater.inflate(R.layout.game_history_current_recycler, container, false)
    }

    /**
     * Sets up the recycler once the view has been created.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Assigns the recycler view
        setupRecyclerView(
            GameType.Current,
            getView()!!.findViewById(R.id.current_recycler) as RecyclerView,
            view,
            activity
        )
    }
}
package com.namethatsongswansea.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.namethatsongswansea.R
import com.namethatsongswansea.activities.adapter.TabsPagerAdapter

class GameHistoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_history_activity)

        // Initialises UI elements
        initialiseUi()
    }

    /**
     * This function sets up and initialises some UI
     * elements when the activity is created.
     */
    private fun initialiseUi() {

        // Sets up the Toolbar / Appbar
        setupAppBar()

        // Sets up the two tabs
        setupTabs()
    }

    /**
     * Sets up the Toolbar / Appbar
     */
    private fun setupAppBar() {

        // Initialises the Toolbar / Appbar
        val mToolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(mToolbar)

        // Customises the tool bar
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.appbar_title)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
    }

    /**
     * Sets up the two tabs.
     */
    private fun setupTabs() {

        // Collects the UI elements
        val tabLayout = findViewById<TabLayout>(R.id.tab_layout)
        val viewPager = findViewById<ViewPager>(R.id.pager)
        val tabTitles = resources.getStringArray(R.array.tabTitles)
        val mAdapter = TabsPagerAdapter(supportFragmentManager, tabTitles, this)

        // Sets the adapter and adds the on page change listener.
        viewPager.adapter = mAdapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        // Sets a listener for when the tabs are clicked / selected.
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab?) {
                viewPager.currentItem = tab!!.position
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}
        })
    }
}
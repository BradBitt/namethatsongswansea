package com.namethatsongswansea.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.namethatsongswansea.MasterActivity
import com.namethatsongswansea.R
import com.namethatsongswansea.game.GameRunner
import com.namethatsongswansea.game.GameType
import com.namethatsongswansea.resources.PreviousGameManager
import com.namethatsongswansea.resources.ResourceManager
import com.namethatsongswansea.resources.ResourceManager.deleteResources
import com.namethatsongswansea.resources.UserInfoManager
import com.namethatsongswansea.util.Permissions

class HomeActivity : MasterActivity() {

    /**
     * This function is called when the view is created.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)

        // Asks for Permissions
        Permissions.ask(this)

        // Updates the total score label
        showTotalScoreValue()
    }

    /**
     * This function is called to update the UI.
     */
    override fun onStart() {
        super.onStart()

        // Updates the total score label
        showTotalScoreValue()
    }

    /**
     * This function is called to update the UI.
     */
    override fun onResume() {
        super.onResume()

        // Updates the total score label
        showTotalScoreValue()
    }

    /**
     * When the main activity is paused then
     * it saves the resources to file.
     */
    override fun onStop() {
        super.onStop()

        // Saves resources
        ResourceManager.saveResources(this)
    }

    /**
     * When the main Activity is destroyed,
     * All resources are saved.
     */
    override fun onDestroy() {
        super.onDestroy()

        // Saves resources
        ResourceManager.saveResources(this)
    }

    /**
     * This function launches the current guessing game.
     */
    @Suppress("UNUSED_PARAMETER")
    fun launchCurrentGuessGame(view: View) {

        // Checks if permissions have been given.
        if (!Permissions.check(this)) {
            MaterialAlertDialogBuilder(this)
                .setTitle(getString(R.string.no_permissions_title))
                .setPositiveButton(getString(R.string.no_permissions_ok), null)
                .show()
            return
        }

        // Runs the game runner to get the game
        GameRunner.startGameNewGame(this, GameType.Current)
    }

    /**
     * This function runs the classic guessing game.
     */
    @Suppress("UNUSED_PARAMETER")
    fun launchClassicGuessGame(view: View) {

        // Checks if permissions have been given.
        if (!Permissions.check(this)) {
            MaterialAlertDialogBuilder(this)
                .setTitle(getString(R.string.no_permissions_title))
                .setPositiveButton(getString(R.string.no_permissions_ok), null)
                .show()
            return
        }

        // Runs the game runner to get the game
        GameRunner.startGameNewGame(this, GameType.Classic)
    }

    /**
     * This function launches the Game History Activity.
     */
    @Suppress("UNUSED_PARAMETER")
    fun launchGameHistory(view: View) {

        // Launches the activity
        val i = Intent(this, GameHistoryActivity::class.java)
        startActivity(i)
    }

    /**
     * This function is called by the
     * 'share' button. This shares the users
     * total score with Facebook.
     */
    fun shareTotalScore(view: View) {

        //val callbackManager = CallbackManager.Factory.create()
        val shareDialog = ShareDialog(this)

        // Creates the share object and opens it.
        val linkContent = ShareLinkContent.Builder()
            .setQuote(getString(R.string.share_total_score_message) + " " + UserInfoManager.getScore().toString())
            .setContentUrl(Uri.parse(view.context.getString(R.string.bitbucket_link)))
            .build()
        try {
            shareDialog.show(linkContent)
        } catch (e: Exception) {
            Snackbar.make(view, getString(R.string.facebook_share_fail), Snackbar.LENGTH_LONG)
                .show()
        }
    }

    /**
     * This function is activated by the 'reset account'
     * button on the UI. It resets the users account and so
     * removes the Game history and total points.
     */
    fun resetAccount(view: View) {

        // Deletes the resources
        deleteResources(this)
        UserInfoManager.resetScore()
        PreviousGameManager.setRecentGame(null)

        // Reloads the resource manager
        ResourceManager.setup(this)

        // Creates UI Alert
        Snackbar.make(view, getString(R.string.reset_account_snack), Snackbar.LENGTH_LONG).show()

        // Updates UI
        showTotalScoreValue()
    }

    /**
     * This function updates the total points label
     * with the current total points.
     */
    private fun showTotalScoreValue() {
        val totalPointsContainer = findViewById<TextView>(R.id.TotalPointsContainer)
        totalPointsContainer.text = UserInfoManager.getScore().toString()
    }
}
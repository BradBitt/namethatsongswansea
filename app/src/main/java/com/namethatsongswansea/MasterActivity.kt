package com.namethatsongswansea

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.facebook.FacebookSdk
import com.namethatsongswansea.resources.ResourceManager

open class MasterActivity : AppCompatActivity() {

    /**
     * This function is called when
     * an activity in this app is created.
     *
     * This function sets up the resource manager.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Initialises the facebook API
        FacebookSdk.sdkInitialize(this.applicationContext)

        // Sets up the resource manager
        ResourceManager.checkStateOfManager(this)
    }

    /**
     * This function is called when an
     * activity is started. This can happen when
     * an activity is restarted.
     *
     * This function makes sure that the resource manager
     * is in the correct state and no data is missing.
     */
    override fun onStart() {
        super.onStart()

        // checks the state of the manager
        ResourceManager.checkStateOfManager(this)
    }

    /**
     * This function is called when an
     * activity is resumed. This can happen when
     * an activity is paused.
     *
     * This function makes sure that the resource manager
     * is in the correct state and no data is missing.
     */
    override fun onResume() {
        super.onResume()

        // checks the state of the manager
        ResourceManager.checkStateOfManager(this)
    }
}
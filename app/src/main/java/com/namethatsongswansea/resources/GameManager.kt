package com.namethatsongswansea.resources

import com.namethatsongswansea.game.GameType
import com.namethatsongswansea.game.lyric.LyricGame

object GameManager {

    // This key is used to pass the name of objects between activities.
    const val LYRIC_GAME_TITLE_KEY = "title_key"

    // All Games loaded in the mobile game.
    var allLyricGames: ArrayList<LyricGame> = arrayListOf<LyricGame>()

    /**
     * Adds a completed game to the array of completed games.
     */
    fun addCompletedGame(game: LyricGame) {

        // If the lyricContainer game already exists then replace it.
        for (lyricGame in allLyricGames) {

            // If the two lyrics are equal
            if (lyricGame.lyricContainer.title.equals(game.lyricContainer.title)) {

                // Remove the previous one and add the new one, then exit function.
                allLyricGames.remove(lyricGame)
                allLyricGames.add(game)
                return
            }
        }

        // If the lyricContainer could not be found then just add it.
        allLyricGames.add(game)
    }

    /**
     * This function returns a list of all the completed games.
     */
    fun getCompletedGames(): ArrayList<LyricGame> {

        // Creates an empty array list
        val allCompletedGames = arrayListOf<LyricGame>()

        // Loops through all the games and adds the completed ones to
        // the new array
        for (lyricGame in allLyricGames) {
            if (lyricGame.isCompleted) {
                allCompletedGames.add(lyricGame)
            }
        }

        return allCompletedGames
    }

    /**
     * This function returns a list of all the uncompleted games.
     */
    fun getUncompletedGames(): ArrayList<LyricGame> {

        // Creates an empty array list
        val allUncompletedGames = arrayListOf<LyricGame>()

        // Loops through all the games and adds the uncompleted ones to
        // the new array
        for (lyricGame in allLyricGames) {
            if (!lyricGame.isCompleted) {
                allUncompletedGames.add(lyricGame)
            }
        }

        return allUncompletedGames
    }

    /**
     * Collects a random uncompleted game and returns it.
     */
    fun getRandomUncompletedGame(gameType: GameType): LyricGame? {

        // Gets a temp array
        val tempArray = arrayListOf<LyricGame>()
        for (lyricGame in getUncompletedGames()) {
            tempArray.add(lyricGame)
        }

        // Shuffles the array and returns the first element.
        tempArray.shuffle()

        // Finds the first uncompleted game that is of the same type.
        for (game in tempArray) {
            if (game.gameType == gameType) {
                return game
            }
        }
        return null
    }

    /**
     * Finds a game by the title provided and returns it.
     */
    fun findGameByTitle(title: String): LyricGame? {

        // Loops through all the games and tries to find the correct one.
        for (game in allLyricGames) {
            if (game.lyricContainer.title.equals(title)) {
                return game
            }
        }

        // Game could not be found.
        return null
    }
}
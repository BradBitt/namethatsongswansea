package com.namethatsongswansea.resources

import android.content.Context
import com.namethatsongswansea.game.lyric.LyricGame

object PreviousGameManager {

    // The previous game. Can be null.
    // If null then there was no previous game.
    // This is set when a game is left without being finished.
    // It is then set to null once the game has been finished.
    private var recentGame: LyricGame? = null

    /**
     * Gets the previous game.
     */
    fun getRecentGame(): LyricGame? {
        return recentGame
    }

    /**
     * Sets the previous game.
     */
    fun setRecentGame(newRecentGame: LyricGame?) {
        recentGame = newRecentGame
    }

    /**
     * This function removes the previous game.
     */
    fun removeGame(context: Context) {

        // Removes the current recent game
        recentGame = null

        // Deletes the file
        context.deleteFile(ResourceManager.PREVIOUS_GAME_FILE)
    }
}
package com.namethatsongswansea.resources

object UserInfoManager {

    private var totalScore = 0

    /**
     * This is used when the program is started up and
     * loads in the total score.
     */
    fun loadScore(newScore: Int) {
        totalScore = newScore
    }

    /**
     * This is used to append a completed
     * game score to the total score.
     */
    fun appendScore(gameScore: Int) {
        totalScore += gameScore
    }

    /**
     * This function returns the total score.
     */
    fun getScore(): Int {
        return totalScore
    }

    /**
     * This function resets the users score.
     */
    fun resetScore() {
        totalScore = 0
    }

}
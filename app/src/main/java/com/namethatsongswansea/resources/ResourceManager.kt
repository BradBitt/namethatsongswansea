package com.namethatsongswansea.resources

import android.content.res.AssetManager
import androidx.appcompat.app.AppCompatActivity
import com.namethatsongswansea.game.GameType
import com.namethatsongswansea.game.lyric.LyricContainer
import com.namethatsongswansea.game.lyric.LyricGame
import com.namethatsongswansea.util.FileLoader
import com.namethatsongswansea.util.XMLFileReader
import com.namethatsongswansea.util.XMLFileWriter
import java.io.File

object ResourceManager {

    // Static known variables
    const val CLASSIC_DIR: String = "classic"
    const val CURRENT_DIR: String = "current"
    const val USER_INFO_FILE: String = "user_info.xml"
    const val GAME_HISTORY_FILE: String = "game_history.xml"
    const val PREVIOUS_GAME_FILE: String = "previous_game.xml"

    // Most recent set of assets
    lateinit var allAssets: AssetManager

    /**
     * This function checks that all the resources are loaded
     * and were not lost.
     */
    fun checkStateOfManager(activity: AppCompatActivity) {

        // If there is no games found then rerun setup.
        if (GameManager.allLyricGames.isEmpty()) {
            setup(activity)
        }
    }

    /**
     * This function saves all the
     * resources within the manager.
     */
    fun saveResources(activity: AppCompatActivity) {

        // Saves the user data
        saveUserData(activity)

        // Saves the game history
        saveGameHistory(activity)

        // Saves previous game
        savePreviousGame(activity)
    }


    /**
     * This function loads all the resources and
     * creates all the resource objects for the
     * mobile application to use.
     */
    fun setup(activity: AppCompatActivity) {

        if (activity.assets == null) {
            throw Exception("No assets found!")
        }

        // Assigns global variables
        this.allAssets = activity.assets

        // Loads the songs from the resource files
        // and generates lyricContainer objects.
        loadAllSongs()

        // Loads user data.
        loadUserData(activity)

        // Loads game history.
        loadGameHistory(activity)

        // Loads previous game data.
        loadPreviousGame(activity)
    }

    /**
     * This function deletes all the resources that are saved onto
     * the mobile device.
     */
    fun deleteResources(activity: AppCompatActivity) {

        // Deletes all the files
        deleteFile(activity, this.USER_INFO_FILE)
        deleteFile(activity, this.GAME_HISTORY_FILE)
        deleteFile(activity, this.PREVIOUS_GAME_FILE)
    }

    /**
     * This function deletes a given file from the mobile
     * internal storage.
     */
    private fun deleteFile(activity: AppCompatActivity, filename: String) {
        activity.deleteFile(filename)
    }


    /**
     * This function loads all the songs from the
     * text files.
     */
    private fun loadAllSongs() {

        // All lyricContainer games objects
        val allLyricGames: ArrayList<LyricGame> = arrayListOf<LyricGame>()

        // Gets all files in a directory.
        val classicFiles = allAssets.list(this.CLASSIC_DIR)
        val currentFiles = allAssets.list(this.CURRENT_DIR)

        // Uses the file loader to get all the songs.
        val classicLyrics = FileLoader.loadSongs(classicFiles, this.CLASSIC_DIR)
        val currentLyrics = FileLoader.loadSongs(currentFiles, this.CURRENT_DIR)

        // Converts to LyricGames
        allLyricGames.addAll(convertToLyricGames(classicLyrics, GameType.Classic))
        allLyricGames.addAll(convertToLyricGames(currentLyrics, GameType.Current))

        // Adds all of them to the Game Manager
        for (lyricGame in allLyricGames) {
            GameManager.addCompletedGame(lyricGame)
        }
    }

    /**
     * Converts the given list of lyricContainer objects to LyricGame objects.
     */
    private fun convertToLyricGames(
        arrayOfLyricContainers: Array<LyricContainer>,
        gameType: GameType
    ): ArrayList<LyricGame> {

        // Creates a new array ot return
        val allLyricGames: ArrayList<LyricGame> = arrayListOf<LyricGame>()

        // loops through all the elements in the array
        for (lyric in arrayOfLyricContainers) {
            allLyricGames.add(LyricGame(0, lyric, gameType))
        }

        return allLyricGames
    }

    /**
     * This function loads the saved user data.
     * If the user data cannot be found then the user data is created.
     */
    private fun loadUserData(activity: AppCompatActivity) {

        val userInfoFile = File(activity.filesDir, this.USER_INFO_FILE)
        if (!userInfoFile.exists()) {
            // If the file does not exist
            // then use the default data in UserInfoManager
            return
        }

        XMLFileReader.setUserInfo(userInfoFile, activity)
    }

    /**
     * This function loads the saved game history data.
     * If the data data cannot be found then blank history data is created.
     */
    private fun loadGameHistory(activity: AppCompatActivity) {

        val gameHistoryFile = File(activity.filesDir, this.GAME_HISTORY_FILE)
        if (!gameHistoryFile.exists()) {
            // If the file does not exist
            // then use the default data in UserInfoManager
            return
        }

        // Reads the data from the file
        XMLFileReader.setGameHistoryInfo(gameHistoryFile, activity)
    }

    /**
     * Loads the previous game from the mobile device if a previous game
     * exists.
     */
    private fun loadPreviousGame(activity: AppCompatActivity) {

        val previousGameFile = File(activity.filesDir, this.PREVIOUS_GAME_FILE)
        if (!previousGameFile.exists()) {
            // If the file does not exist
            // then use the default data in UserInfoManager
            return
        }

        // Reads the data from the file
        XMLFileReader.setPreviousGame(previousGameFile, activity)
    }

    /**
     * This function takes the user data from its
     * manager and saves it to the mobile device.
     */
    private fun saveUserData(activity: AppCompatActivity) {

        val userInfoFile = File(activity.filesDir, this.USER_INFO_FILE)

        // Creates the xml and writes it to file.
        XMLFileWriter.writeUserInfo(userInfoFile)
    }

    /**
     * This function takes the game history
     * from its manager and saves it to the mobile device.
     */
    private fun saveGameHistory(activity: AppCompatActivity) {

        val gameHistoryFile = File(activity.filesDir, this.GAME_HISTORY_FILE)

        // Creates the xml and writes it to file.
        XMLFileWriter.writeGameHistory(gameHistoryFile)
    }

    /**
     * Saves the previous game as a file on the mobile device.
     * It only does this if a previous game exists.
     */
    private fun savePreviousGame(activity: AppCompatActivity) {

        val prevGame: LyricGame? = PreviousGameManager.getRecentGame()

        // If previous game is null then don't save anything
        // and make sure the previous game file does not exist.
        if (prevGame == null) {

            deleteFile(activity, this.PREVIOUS_GAME_FILE)
            return
        }

        val previousGameFile = File(activity.filesDir, this.PREVIOUS_GAME_FILE)

        // Gets the previous game and writes it to file.
        XMLFileWriter.writePreviousGame(previousGameFile, prevGame)
    }
}
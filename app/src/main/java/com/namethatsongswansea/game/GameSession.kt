package com.namethatsongswansea.game

import android.graphics.Color
import android.util.TypedValue
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.isVisible
import com.google.android.flexbox.FlexboxLayout
import com.google.android.material.button.MaterialButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputLayout
import com.namethatsongswansea.activities.GuessingGameActivity
import com.namethatsongswansea.game.lyric.Lyric
import com.namethatsongswansea.game.lyric.LyricGame
import com.namethatsongswansea.resources.PreviousGameManager

class GameSession(
    private val activity: GuessingGameActivity,
    var lyricGame: LyricGame?
) {

    // A list of all lyricContainer locations
    val allLyrics = arrayListOf<Lyric>()

    /**
     * This function is called when the game session is created.
     */
    init {

        // If the game is null then throw an exception.
        if (lyricGame == null) throw Exception("Game within Game Session does not exist!")

        // Updates the game UI.
        updateGameUI()

        // Creates LyricContainer Areas for each lyricContainer that is not discovered.
        for (lyric in lyricGame!!.lyricContainer.getAllUndiscoveredLyrics()) {
            allLyrics.add(lyric)
        }
    }

    /**
     * This function updates all the UI in the activity.
     */
    fun updateGameUI() {
        // Updates the score and game type..
        val scoreLabel =
            activity.findViewById<View>(com.namethatsongswansea.R.id.score_label) as TextView
        scoreLabel.text =
            activity.getString(com.namethatsongswansea.R.string.score_label) + " " + lyricGame!!.getScore().toString()
        val gameTypeLabel =
            activity.findViewById<View>(com.namethatsongswansea.R.id.game_type_label) as TextView
        gameTypeLabel.text =
            activity.getString(com.namethatsongswansea.R.string.type_label) + " " + lyricGame!!.gameType.toString()

        // Remakes lyricContainer boxes
        val lyricsContainer =
            activity.findViewById<View>(com.namethatsongswansea.R.id.lyrics_container) as FlexboxLayout
        lyricsContainer.removeAllViews()
        makeLyricBoxUI(lyricsContainer)

        // Updates the buttons
        updateButtons()
    }

    /**
     * This function is called when the game has been given up by the user.
     */
    fun removeGame() {

        // Sets game to null and removes the previous game.
        lyricGame = null
        PreviousGameManager.removeGame(activity)

        // Quites the activity.
        activity.finish()
    }

    /**
     * This function is called if the user discovers a lyric on the map.
     * It reveals the lyric and adds points to the user.
     */
    fun lyricDiscovered(lyric: Lyric) {

        // Adds the points and discovers the lyric
        if (!lyric.discovered) {
            lyricGame!!.updateScore(PointValues.LYRIC_DISCOVERY)
            lyric.discovered = true
        }

        // Updates the UI
        lyricGame!!.hasFirstLyricBeenDiscovered = true
        updateGameUI()

        // Removes the marker from the map
        activity.gameMap.removeDiscoveredLyric(lyric)
    }

    /**
     * This function updates all the UI buttons.
     */
    private fun updateButtons() {

        // Updates the Artist button
        val artistButton =
            activity.findViewById<View>(com.namethatsongswansea.R.id.artist_button) as FloatingActionButton
        if (lyricGame!!.isArtistCorrect) {
            artistButton.setImageResource(com.namethatsongswansea.R.drawable.correct_artist_24px)
            artistButton.isClickable = false
        }

        // Updates the help button, makes it clickable if there are still discoverable words.
        val helpButton =
            activity.findViewById<View>(com.namethatsongswansea.R.id.help_button) as FloatingActionButton
        helpButton.isClickable = !(lyricGame!!.areAllLyricsFound())

        // Updates the two main action buttons
        val guessButton =
            activity.findViewById<View>(com.namethatsongswansea.R.id.guess_button) as MaterialButton
        val completeButton =
            activity.findViewById<View>(com.namethatsongswansea.R.id.win_button) as MaterialButton
        if (lyricGame!!.isCompleted) {

            // Hide guess button
            changeButtonVisibility(guessButton, false)

            // Show complete button
            changeButtonVisibility(completeButton, true)
        } else {

            // Checks if any lyrics have been found
            if (lyricGame!!.hasFirstLyricBeenDiscovered) {

                // Show guess button
                changeButtonVisibility(guessButton, true)

                // Hide complete button
                changeButtonVisibility(completeButton, false)
            } else {

                // Hide guess button
                changeButtonVisibility(guessButton, false)

                // Hide complete button
                changeButtonVisibility(completeButton, false)
            }
        }
    }

    /**
     * This function makes the lyricContainer text box UI.
     */
    private fun makeLyricBoxUI(lyricsContainer: FlexboxLayout) {

        // Adds the boxes to the UI as the form of text views and text inputs
        for (lyric in lyricGame!!.lyricContainer.lyrics) {

            // Makes a text view if it has been discovered.
            if (lyric.discovered) {

                // Makes text view
                createTextView(lyric, lyricsContainer)
            } else {

                // Makes the Text input
                createTextInput(lyricsContainer)
            }
        }
    }

    /**
     * This function makes a text view for the lyricContainer container
     * and also styles the text view correctly.
     */
    private fun createTextView(lyric: Lyric, lyricsContainer: FlexboxLayout) {

        // Makes text view
        val lyricTextView = TextView(activity)
        lyricTextView.text = lyric.lyric

        lyricTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 35.toFloat())
        lyricTextView.setTextColor(activity.resources.getColor(com.namethatsongswansea.R.color.textColorPrimary))

        // Sets the margin, width and height
        val params =
            LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
        params.setMargins(10, 0, 10, 10)
        lyricTextView.layoutParams = params

        // Sets the parent container for the text view.
        lyricsContainer.addView(lyricTextView)
    }

    /**
     * This function makes the text input object that goes into
     * the lyrics container.
     */
    private fun createTextInput(lyricsContainer: FlexboxLayout) {

        // Makes the text input
        val textInput = TextInputLayout(activity)

        // Makes the edit text which goes inside the text input
        val editText = EditText(activity)

        // Styles the text input
        val params =
            LinearLayout.LayoutParams(
                com.namethatsongswansea.R.dimen.lyric_box_text_input_width,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
        textInput.layoutParams = params
        textInput.boxStrokeColor = Color.BLACK

        // Styles the edit text
        editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 35.toFloat())
        editText.setBackgroundColor(Color.TRANSPARENT)
        editText.isEnabled = false
        editText.isClickable = false

        // Sets the parent for the edit text
        textInput.addView(editText)

        // Sets the parent container for the text view.
        lyricsContainer.addView(textInput)
    }

    /**
     * This function updates a givens button visibility and whether it is enabled or not
     * by the given boolean value
     */
    private fun changeButtonVisibility(button: MaterialButton, bool: Boolean) {
        button.isVisible = bool
        button.isEnabled = bool
    }
}
package com.namethatsongswansea.game.listener

import android.content.DialogInterface
import android.widget.EditText
import com.namethatsongswansea.activities.GuessingGameActivity
import com.namethatsongswansea.game.lyric.LyricGame
import com.namethatsongswansea.game.PointValues

class ArtistDialogClickListener(
    private val activity: GuessingGameActivity,
    private val artistInput: EditText,
    private val game: LyricGame
) : DialogInterface.OnClickListener {

    /**
     * This function is called when the user clicks the guess button
     * in the guess artist dialog window.
     */
    override fun onClick(dialog: DialogInterface, which: Int) {

        // Checks if any text has been entered in
        if (artistInput.text.isEmpty()) {
            return
        }

        // Checks to see if the text entered is the same as the text in the lyricContainer game.
        // The whitespace and casing of the text is removed so they can be compared easier.
        val gameArtist = game.lyricContainer.artist.toLowerCase().replace(" ", "")
        val guessArtist = artistInput.text.toString().toLowerCase().replace(" ", "")
        if (gameArtist.equals(guessArtist)) {

            // Updates the game to have artist complete
            game.isArtistCorrect = true

            // Adds the points
            game.updateScore(PointValues.ARTIST_GUESSED)
        } else {

            // Provides the negative points for failing.
            game.updateScore(PointValues.INCORRECT_GUESS)
        }

        // Updates the UI
        activity.session.updateGameUI()
    }
}
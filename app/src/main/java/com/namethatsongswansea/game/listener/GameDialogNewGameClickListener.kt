package com.namethatsongswansea.game.listener

import android.content.Context
import android.content.DialogInterface
import com.namethatsongswansea.game.GameRunner
import com.namethatsongswansea.game.GameType
import com.namethatsongswansea.resources.PreviousGameManager

class GameDialogNewGameClickListener(
    private val context: Context,
    private val clickType: GameRunner.ClickType,
    private val gameType: GameType
) : DialogInterface.OnClickListener {

    /**
     * This function is called when a user clicks to play a new game
     * and was asked weather they want to carry on playing the previous game.
     */
    override fun onClick(dialog: DialogInterface, which: Int) {

        // Checks what type of onclick this is
        // If the click type is 'remove'
        if (clickType == GameRunner.ClickType.Remove) {
            removeClicked(context, gameType)
        } else {
            GameRunner.continueGame(context)
        }
    }

    /**
     * This function removes the previous game and starts a new game.
     * Using the game type provided by the user.
     */
    private fun removeClicked(context: Context, gameType: GameType) {

        // Removes the previous game
        PreviousGameManager.removeGame(context)

        // Creates a new Game
        GameRunner.createNewGame(context, gameType)
    }
}
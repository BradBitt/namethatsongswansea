package com.namethatsongswansea.game.listener

import android.content.Context
import android.content.DialogInterface
import com.namethatsongswansea.game.GameRunner
import com.namethatsongswansea.game.lyric.LyricGame
import com.namethatsongswansea.resources.PreviousGameManager

class GameDialogWithGameClickListener(
    private val context: Context,
    private val clickType: GameRunner.ClickType,
    private val game: LyricGame
) : DialogInterface.OnClickListener {

    /**
     * This function is called when the user wants to replay a game
     * and was asked if they want to carry on playing the previous game.
     */
    override fun onClick(dialog: DialogInterface, which: Int) {

        // Checks what type of onclick this is
        // If the click type is 'remove'
        if (clickType == GameRunner.ClickType.Remove) {
            removeClicked(context, game)
        } else {
            GameRunner.continueGame(context)
        }
    }

    /**
     * This function removes the previous game and plays the
     * game provided.
     */
    private fun removeClicked(context: Context, game: LyricGame) {

        // Removes the previous game
        PreviousGameManager.removeGame(context)

        // Creates a new Game
        GameRunner.runGameActivity(context, game.lyricContainer.title)
    }
}
package com.namethatsongswansea.game.listener

import android.content.DialogInterface
import android.widget.EditText
import com.namethatsongswansea.activities.GuessingGameActivity
import com.namethatsongswansea.game.lyric.LyricGame
import com.namethatsongswansea.game.PointValues

class GuessDialogClickListener(
    private val activity: GuessingGameActivity,
    private val titleInput: EditText,
    private val game: LyricGame
) : DialogInterface.OnClickListener {

    /**
     * This function is called when the user decides to enter a guess for the song
     * title.
     */
    override fun onClick(dialog: DialogInterface, which: Int) {

        // Checks if any text has been entered in
        if (titleInput.text.isEmpty()) {
            return
        }

        // Checks to see if the text entered is the same as the text in the lyricContainer game.
        // The whitespace and casing of the text is removed so they can be compared easier.
        val gameTitle = game.lyricContainer.title.toLowerCase().replace(" ", "")
        val guessTitle = titleInput.text.toString().toLowerCase().replace(" ", "")
        if (gameTitle.equals(guessTitle)) {

            // Updates the game to complete
            game.isCompleted = true

            // Adds the points
            PointValues.titleGuessed(game)
        } else {

            // Provides the negative points for failing.
            game.updateScore(PointValues.INCORRECT_GUESS)
        }

        // Updates the UI
        activity.session.updateGameUI()
    }
}
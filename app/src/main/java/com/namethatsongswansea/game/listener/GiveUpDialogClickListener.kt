package com.namethatsongswansea.game.listener

import android.content.DialogInterface
import com.namethatsongswansea.activities.GuessingGameActivity
import com.namethatsongswansea.game.lyric.LyricGame
import com.namethatsongswansea.game.PointValues
import com.namethatsongswansea.resources.UserInfoManager

/**
 * This class is the onclick listener for the give up dialog window.
 */
class GiveUpDialogClickListener(val activity: GuessingGameActivity, val game: LyricGame) :
    DialogInterface.OnClickListener {

    /**
     * This function is called when the user clicks the give
     * up button within the dialog window.
     */
    override fun onClick(dialog: DialogInterface, which: Int) {

        // Awards the negative points
        PointValues.giveUp(game)

        // Adds the points to the users total points.
        UserInfoManager.appendScore(game.getScore())

        // runs the give up function in the activity.
        activity.session.removeGame()
    }
}
package com.namethatsongswansea.game

import android.content.Context
import android.content.Intent
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.namethatsongswansea.R
import com.namethatsongswansea.activities.GuessingGameActivity
import com.namethatsongswansea.game.listener.GameDialogNewGameClickListener
import com.namethatsongswansea.game.listener.GameDialogWithGameClickListener
import com.namethatsongswansea.game.lyric.LyricGame
import com.namethatsongswansea.resources.GameManager
import com.namethatsongswansea.resources.PreviousGameManager

object GameRunner {

    /**
     * An Enum object to show what type of on click listener this is.
     */
    enum class ClickType {
        Remove,
        Continue
    }

    /**
     * This function starts the game.
     * It creates a new game if there is not a previous one.
     */
    fun startGameNewGame(parentContext: Context, gameType: GameType) {

        // Checks to see if there is a previous game
        if (PreviousGameManager.getRecentGame() == null) {

            // Creates a new game
            createNewGame(parentContext, gameType)
        } else {

            // Asks the user if they want to load up the previous game
            askToPlayPreviousGameNewGame(parentContext, gameType)
        }
    }

    /**
     * This function runs a game that has already been completed.
     */
    fun startGameWithGame(parentContext: Context, game: LyricGame) {

        // Checks to see if there is a previous game
        if (PreviousGameManager.getRecentGame() == null) {

            // Runs a new game with the lyricContainer game attached
            runGameActivity(parentContext, game.lyricContainer.title)
        } else {

            // Asks the user if they want to load up the previous game
            // includes a game that will be played based on what the user says.
            askToPlayPreviousGameWithGame(parentContext, game)
        }
    }

    /**
     * This function creates a new game.
     */
    fun createNewGame(parentContext: Context, gameType: GameType) {

        // Finds an uncompleted game.
        var game = GameManager.getRandomUncompletedGame(gameType)

        // Gets all the games and shuffles them.
        val allGames = arrayListOf<LyricGame>()
        for (lyricGame in GameManager.allLyricGames) {
            allGames.add(lyricGame)
        }
        allGames.shuffle()

        // If the game is null then a already completed game with the same type.
        if (game == null) {

            for (gameElem in allGames) {
                if (gameElem.gameType == gameType) {
                    game = gameElem
                }
            }
        }

        // Runs the guessing game activity.
        runGameActivity(parentContext, game!!.lyricContainer.title)
    }

    /**
     * This function runs the previous game
     * if one exists.
     */
    fun continueGame(parentContext: Context) {

        // Gets the previous game.
        val previousGame = PreviousGameManager.getRecentGame()

        // Runs the guessing game activity.
        runGameActivity(parentContext, previousGame!!.lyricContainer.title)
    }

    /**
     * Runs the guessing game activity with the
     * game object title being passed through.
     */
    fun runGameActivity(parentContext: Context, lyricTitle: String) {

        // Creates the intent
        val intent = Intent(parentContext, GuessingGameActivity::class.java)
        intent.putExtra(GameManager.LYRIC_GAME_TITLE_KEY, lyricTitle)

        // Runs the guessing game activity
        parentContext.startActivity(intent)
    }


    /**
     * This function create a pop up and
     * asks the user if they want to continue the previous game
     * or start a new one.
     */
    private fun askToPlayPreviousGameNewGame(context: Context, gameType: GameType) {
        MaterialAlertDialogBuilder(context)
            .setTitle(context.getString(R.string.previous_game_dialog_title))
            .setPositiveButton(
                R.string.previous_game_dialog_button_continue,
                GameDialogNewGameClickListener(
                    context,
                    ClickType.Continue,
                    gameType
                )
            )
            .setNegativeButton(
                R.string.previous_game_dialog_button_remove,
                GameDialogNewGameClickListener(
                    context,
                    ClickType.Remove,
                    gameType
                )
            )
            .show()
    }

    /**
     * This function create a pop up and
     * asks the user if they want to continue the previous game
     * or start a new one.
     * Includes a LyricContainer Game and will run this.
     */
    private fun askToPlayPreviousGameWithGame(context: Context, game: LyricGame) {
        MaterialAlertDialogBuilder(context)
            .setTitle(context.getString(R.string.previous_game_dialog_title))
            .setPositiveButton(
                R.string.previous_game_dialog_button_continue,
                GameDialogWithGameClickListener(
                    context,
                    ClickType.Continue,
                    game
                )
            )
            .setNegativeButton(
                R.string.previous_game_dialog_button_remove,
                GameDialogWithGameClickListener(
                    context,
                    ClickType.Remove,
                    game
                )
            )
            .show()
    }
}
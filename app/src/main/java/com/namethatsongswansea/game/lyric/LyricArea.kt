package com.namethatsongswansea.game.lyric

import com.google.android.gms.maps.model.LatLng

class LyricArea(
    private val longitudeRightBoundary: Double,
    private val longitudeLeftBoundary: Double,
    private val latitudeTopBoundary: Double,
    private val latitudeBottomBoundary: Double
) {

    /**
     * This function checks to see that the location provided is within
     * the 4 boundaries. If the location is within the boundaries then
     * the function returns true. Otherwise the function returns false.
     */
    fun isInBoundary(location: LatLng): Boolean {

        // Checks to see if the location given is in between
        // the longitude values and latitude values
        if (location.latitude < latitudeTopBoundary &&
            location.latitude > latitudeBottomBoundary &&
            location.longitude < longitudeRightBoundary &&
            location.longitude > longitudeLeftBoundary
        ) {
            return true
        }
        return false
    }
}
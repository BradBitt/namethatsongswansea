package com.namethatsongswansea.game.lyric

class LyricContainer(var title: String, var artist: String, var lyricSnippet: String) {

    // Contains a list of all the lyricContainer boxes.
    var lyrics = arrayListOf<Lyric>()

    companion object {
        const val NUMBER_OF_LYRICS_TO_UNDISCOVER = 2
    }

    /**
     * This function is called when the lyricContainer game object is built.
     * It reads in the lyrics and breaks them down into lyrics.
     */
    init {

        // Breaks the lyricContainer snippet string into a list of words.
        val lyrics = lyricSnippet.split(" ")

        // Creates a lyricContainer box for each lyricContainer.
        // And includes the index as an ID.
        for ((index, lyric) in lyrics.withIndex()) {
            this.lyrics.add(Lyric(lyric, index))
        }

        // Randomises which lyricContainer boxes to be visible
        randomiseLyricBoxesVisibility()
    }

    /**
     * This function is called after the lyricContainer boxes are created.
     * It randomises which lyricContainer boxes should be visible and which ones
     * shouldn't be.
     */
    private fun randomiseLyricBoxesVisibility() {

        // Gets all the lyricContainer boxes.
        val allLyricBoxes = arrayListOf<Lyric>()
        for (lyricBox in lyrics) {
            allLyricBoxes.add(lyricBox)
        }
        allLyricBoxes.shuffle()

        // Sets all the boxes as visible except one.
        // The -1 value, is used as arrays start at 0 and not 1.
        val numberOfBoxesToSetVisible = allLyricBoxes.size - NUMBER_OF_LYRICS_TO_UNDISCOVER - 1
        var counter = 0

        // Sets a random number of the boxes visible.
        while (counter <= numberOfBoxesToSetVisible) {

            // Finds the lyricContainer box equal to this one and then sets it to discovered
            for (lyricBox in lyrics) {
                if (lyricBox == allLyricBoxes[counter]) {
                    lyricBox.discovered = true
                }
            }

            counter++
        }
    }

    /**
     * This function returns a list of all the strings
     * within the lyricContainer snippet that have not been discovered yet.
     */
    fun getAllUndiscoveredLyrics(): ArrayList<Lyric> {
        val allUndiscoveredLyrics = arrayListOf<Lyric>()

        // Loops through all the lyrics and
        // for each one that is not discovered, at it to the string array.
        for (lyric in lyrics) {
            if (!lyric.discovered) {
                allUndiscoveredLyrics.add(lyric)
            }
        }

        return allUndiscoveredLyrics
    }
}
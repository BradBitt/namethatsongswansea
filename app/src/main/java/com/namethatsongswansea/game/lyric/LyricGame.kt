package com.namethatsongswansea.game.lyric

import com.namethatsongswansea.game.GameType

class LyricGame(
    private var score: Int,
    val lyricContainer: LyricContainer,
    val gameType: GameType
) {

    // Whether or not the game has been completed.
    var isCompleted = false

    // Whether the user has correctly named the artist
    var isArtistCorrect = false

    // A boolean on whether the first lyricContainer has been found
    var hasFirstLyricBeenDiscovered = false

    /**
     * This constructor is used to create a lyricContainer object
     * that has already been completed. This is useful when
     * loading completed lyricContainer games from resource files.
     */
    constructor(
        score: Int,
        lyricContainer: LyricContainer,
        gameType: GameType,
        isCompleted: Boolean,
        isArtistCorrect: Boolean
    ) : this(score, lyricContainer, gameType) {

        // Sets the variables.
        this.isCompleted = isCompleted
        this.isArtistCorrect = isArtistCorrect
    }

    /**
     * This function takes in the points and adds it to the game score.
     */
    fun updateScore(points: Int) {
        score += points
    }

    /**
     * This function resets the score of the game.
     */
    fun resetScore() {
        score = 0
    }

    /**
     * This function returns the score of the game.
     */
    fun getScore(): Int {
        return score
    }

    /**
     * This function checks to see if all lyrics have been found.
     */
    fun areAllLyricsFound(): Boolean {

        // Loops through all the lyricContainer boxes and tries to find one that is not discovered.
        for (lyricBox in lyricContainer.lyrics) {
            if (!lyricBox.discovered) {
                return false
            }
        }

        return true
    }

    /**
     * Resets the game.
     */
    fun reset() {

        resetScore()
        isCompleted = false
        isArtistCorrect = false
        hasFirstLyricBeenDiscovered = false
    }
}


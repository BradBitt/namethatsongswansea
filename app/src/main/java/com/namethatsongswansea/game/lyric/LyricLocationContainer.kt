package com.namethatsongswansea.game.lyric

import com.google.android.gms.maps.model.LatLng
import java.util.*

class LyricLocationContainer {

    /**
     * Constant Values
     */
    companion object {
        const val HORIZONTAL_LEFT_LIMIT = -3.8832
        const val HORIZONTAL_RIGHT_LIMIT = -3.875
        const val VERTICAL_TOP_LIMIT = 51.6193
        const val VERTICAL_BOTTOM_LIMIT = 51.618

        const val LCA_GAP = 0.000100
        const val LDA_GAP = 0.000400
    }

    val location: LatLng
    val lca: LyricArea
    val lda: LyricArea

    /**
     * This function is called when the object is created.
     *
     * This generates a random horizontal and vertical co-ordinate that
     * is within the limits specified. These random co-ordinates create centre
     * of the LyricContainer Collection Area (LCA).
     *
     * A LyricContainer Detection Area is then created around the LCA.
     */
    init {

        // Creates the random number generator object.
        val rand = Random()

        // Creates the two co-ordinates.
        val longitude =
            rand.nextDouble() * (HORIZONTAL_RIGHT_LIMIT - HORIZONTAL_LEFT_LIMIT) + HORIZONTAL_LEFT_LIMIT
        val latitude =
            rand.nextDouble() * (VERTICAL_TOP_LIMIT - VERTICAL_BOTTOM_LIMIT) + VERTICAL_BOTTOM_LIMIT

        // Creates the location with the co-ordinates
        location = LatLng(latitude, longitude)

        // Creates the LCA.
        lca = LyricArea(
            longitude + LCA_GAP,
            longitude - LCA_GAP,
            latitude + LCA_GAP,
            latitude - LCA_GAP
        )

        // Creates the LDA.
        lda = LyricArea(
            longitude + LDA_GAP,
            longitude - LDA_GAP,
            latitude + LDA_GAP,
            latitude - LDA_GAP
        )
    }
}
package com.namethatsongswansea.game.lyric

/**
 * An id is required for a lyric object to identify a lyric from
 * another. This is because its possible to have two words that
 * are the same within a lyric snippet. And so having an ID allows us
 * to distinguish between them rather than using the word itself to
 * identify the object.
 */
class Lyric(val lyric: String, val id: Int) {

    // Whether or not this lyricContainer has been discovered.
    var discovered = false

    // The lyricContainer location
    var lyricLocationContainer = LyricLocationContainer()
}
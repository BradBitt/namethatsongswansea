package com.namethatsongswansea.game

/**
 * This is an enum class that contains the different types of game modes
 * that are included within the mobile game.
 */
enum class GameType {
    Classic,
    Current
}
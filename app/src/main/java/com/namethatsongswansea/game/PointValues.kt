package com.namethatsongswansea.game

import com.namethatsongswansea.game.lyric.LyricGame

object PointValues {

    const val LYRIC_DISCOVERY = 1
    const val ARTIST_GUESSED = 5
    const val INCORRECT_GUESS = -1
    const val ASKED_FOR_HELP = -1

    private const val GAVE_UP = -10
    private const val TITLE_GUESSED = 2

    /**
     * This function is called when the user has given up the game.
     * This removes the correct amount of points.
     */
    fun giveUp(game: LyricGame) {
        game.resetScore()
        game.updateScore(GAVE_UP)
    }

    /**
     * This function is called when the user has correctly guessed the title of
     * the game. This adds the correct amount of required points.
     */
    fun titleGuessed(game: LyricGame) {

        // Loops through and awards points for each lyric discovered.
        for (lyric in game.lyricContainer.lyrics) {

            // Checks if it has been discovered
            if (lyric.discovered) {
                game.updateScore(TITLE_GUESSED)
            }
        }
    }
}
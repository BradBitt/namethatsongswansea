package com.namethatsongswansea.game

import android.graphics.BitmapFactory
import android.location.Location
import android.os.Looper
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.namethatsongswansea.activities.GuessingGameActivity
import com.namethatsongswansea.game.lyric.Lyric
import com.namethatsongswansea.util.Permissions

class GameMap(
    private val fusedLocationClient: FusedLocationProviderClient,
    private val activity: GuessingGameActivity
) {

    /**
     * Constant Values
     */
    companion object {
        const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private const val DEFAULT_ZOOM = 18f
    }

    private lateinit var map: GoogleMap
    private lateinit var lastLocation: Location

    private val allMarkers = HashMap<Marker, Lyric>()

    /**
     * A call back variable that when it receives a new location
     * updates the google map camera to point to the new location.
     */
    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            // Gets the new location
            val currentLatLng = LatLng(
                locationResult.lastLocation.latitude,
                locationResult.lastLocation.longitude
            )

            // Updates the camera on the map.
            map.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    currentLatLng,
                    DEFAULT_ZOOM
                )
            )

            // Calls the function to check to see if the users location is within
            // either of the lyric area's for a lyric location.
            checkLyricLocation(currentLatLng)
        }
    }

    /**
     * This function gets the map, updates the UI settings and
     * then sets up the map to work with the guessing game.
     */
    fun initializeMap(googleMap: GoogleMap) {

        map = googleMap

        // disables controls
        map.uiSettings.isZoomControlsEnabled = false
        map.uiSettings.isMyLocationButtonEnabled = false
        map.uiSettings.isMapToolbarEnabled = false

        setUpMap()
    }

    /**
     * This function checks to see if the users location is within any of the lyric areas
     * of any of the undiscovered lyrics.
     */
    private fun checkLyricLocation(latestLocation: LatLng) {

        // Loops through all the markers
        for (marker in allMarkers.keys) {

            // gets the lyric associated to the marker
            val lyric = allMarkers.get(marker) as Lyric

            // If its inside the lyric detection area
            // Then update the marker icon.
            if (lyric.lyricLocationContainer.lda.isInBoundary(latestLocation)) {

                try {
                    marker.setIcon(
                        BitmapDescriptorFactory.fromBitmap(
                            BitmapFactory.decodeResource(
                                activity.resources,
                                com.namethatsongswansea.R.drawable.lca_symbol
                            )
                        )
                    )
                } catch (e: IllegalArgumentException) {
                    print("unable to update icon")
                }
            }

            // If the user is inside the lyric collection area.
            // Then discover the lyric.
            if (lyric.lyricLocationContainer.lca.isInBoundary(latestLocation)) {
                activity.session.lyricDiscovered(lyric)
            }
        }
    }

    /**
     * This function sets up the map to be used by the user.
     */
    private fun setUpMap() {

        // Asks for permission,
        // If we don't get it then quit
        Permissions.ask(activity)
        if (!Permissions.check(activity)) {
            return
        }

        // enables the my-location layer. This draws a light blue circle dot on the users location.
        map.isMyLocationEnabled = true

        // Moves the camera to the users last known location if it can be found.
        fusedLocationClient.lastLocation.addOnSuccessListener(activity) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                map.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        currentLatLng,
                        DEFAULT_ZOOM
                    )
                )
            }
        }

        // Creates an auto update map location listener
        updateUserLocation()
    }

    /**
     * This function collects the updates of
     * user locations and updates the camera of the map.
     */
    private fun updateUserLocation() {

        // Creates a request to check for updates of the user location and
        // then update the map camera whenever the user location changes.
        val locationRequest = LocationRequest.create().apply {
            interval = 2000
            fastestInterval = 1000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        // Creates the location request builder.
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(locationRequest)
        val locationSettingsRequest = builder.build()

        // Updates the clients settings to use the location settings request
        val settingsClient = LocationServices.getSettingsClient(activity)
        settingsClient.checkLocationSettings(locationSettingsRequest)

        // Requests the user location with the variables we created.
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            mLocationCallback,
            Looper.myLooper()
        )
    }

    /**
     * This function removes all the listeners from the map and
     * also removes anything added to the map for a specific game.
     */
    fun stopAndDestroyMap() {

        // Removes the user location listener
        fusedLocationClient.removeLocationUpdates(mLocationCallback)

        // Removes all the markers from the map
        map.clear()
        for (marker in allMarkers.keys) {
            marker.remove()
        }

        // resets the map
        allMarkers.clear()
    }

    /**
     * This function removes the marker from the map that
     * contains the lyric provided.
     */
    fun removeDiscoveredLyric(lyric: Lyric) {

        // Loops through all the markers
        // Removes the marker that matches the lyric.
        for (marker in allMarkers.keys) {
            if (allMarkers.get(marker) == lyric) {
                marker.remove()
            }
        }
    }

    /**
     * This function adds all the markers to the map.
     * Each Marker is a missing lyricContainer.
     */
    fun addMissingLyrics(session: GameSession) {

        // Loops through all the missing lyrics and adds them
        // to the map based on their co-ordinates.
        for (lyric in session.allLyrics) {

            // Creates a location out of the two
            val location = lyric.lyricLocationContainer.location

            // Creates a marker and changes its icon.
            val markerOptions = MarkerOptions().position(location)
            markerOptions.icon(
                BitmapDescriptorFactory.fromBitmap(
                    BitmapFactory.decodeResource(
                        activity.resources,
                        com.namethatsongswansea.R.drawable.lda_static
                    )
                )
            )

            // Creates a marker and adds the lyric as a tag.
            val marker = map.addMarker(markerOptions)

            // Adds marker to map.
            allMarkers.put(marker, lyric)
        }
    }
}
